#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>
#define TYPE gsl_odeiv2_step_rkf45


double g = 1;
double m1 = 1;
double m2 = 1;
double l1 = 1;
double l2 = 1;

int doublependulum(double t, const double y[], double dydt[], void* params);
int main(int argc, char** argv)
{
	int N;
	if(argc>1) {
		N = atol(argv[1]);
	}
	else {
		N = 1000;
	}

	int dim = 4;

	gsl_odeiv2_system s;
	s.function = doublependulum;
	s.jacobian = NULL;
	s.dimension = dim;
	s.params = NULL;


	double h = 0.1;
	double epsabs = 1e-6;
	double epsrel = 1e-6;

	gsl_odeiv2_driver* d = gsl_odeiv2_driver_alloc_y_new(&s,TYPE,h,epsabs,epsrel);

	double timestep = 0.05;
	double t = 0;
	double tstop = N*timestep+t;

	double y[] = {M_PI,M_PI/2,0,0};

	for(double time = t; time < tstop; time += timestep)
	{
//		printf("Advancing from t = %g to t = %g\n",t,time);
		gsl_odeiv2_driver_apply(d, &t, time, y);
		double theta1 = y[0];
		double theta2 = y[1];
		double x1 = l1*sin(theta1);
		double y1 = -l1*cos(theta1);
		double x2 = x1 + l2*sin(theta2);
		double y2 = y1 - l2*cos(theta2);
		printf("%g %g %g %g\n",x1,y1,x2,y2);	
		// TODO Instead of printing thetas, print x1,y1,x2,y2
	}


	return 0;
}
int doublependulum(double t, const double y[], double dydt[], void* params)
{

	double theta1 = y[0];
	double theta2 = y[1];
	double w1 = y[2];
	double w2 = y[3];
	dydt[0] = w1;
	dydt[1] = w2;	

	dydt[2] = -g*(2*m1+m2)*sin(theta1);
	dydt[2] += -m2*g*sin(theta1-2*theta2);
	dydt[2] += -2*sin(theta1-theta2)*m2*(w2*w2*l2+w1*w1*l1*cos(theta1-theta2));
	dydt[2] /= ( l1* ( 2*m1 + m2 - m2*cos(2*theta1-2*theta2) ) );

	dydt[3] = 2*sin(theta1-theta2) * (w1*w1*l1*(m1+m2) + g*(m1+m2)*cos(theta1) + w2*w2*l2*m2*cos(theta1-theta2));
	dydt[3] /= l2*(2*m1+m2-m2*cos(2*theta1-2*theta2));
	return GSL_SUCCESS;
}








































