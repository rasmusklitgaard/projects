PVector origin;
PVector bob1;
PVector bob2;
float l1;
float l2;
float a1 = 2*PI/3;
float a2 = -5*PI/6;
float m1 = 1;
float m2 = 1;
float g = 1;

float w1 = 0;
float w2 = 0;
float a1_a = 0;
float a2_a = 0;

float px2;
float py2;

float density = 1;
float normalarea = 1;
float dragcoeff = 0.47;
float drag = 0.0;


float size1 = 32;
int windowx = 1024;
int windowy = 762;
int translatey = 360;
int colour = 255;
PGraphics canvas;
void setup() {
  size(1024,768);
  canvas = createGraphics(windowx,windowy);
  canvas.beginDraw();
  canvas.background(colour);
  canvas.endDraw();
  l1 = 160;
  l2 = 160;
  origin = new PVector(width/2,0);
  bob1 = new PVector(width/2,l1);
  bob2 = new PVector(width/2,l1+l2);
}
float opsign(float x){
  return -sign(x);
}
float sign(float x) {
  if(x < 0) {
    return -1;
  }
  else{return 1;}
}
void draw() {
  image(canvas,0,0);
  stroke(0);
  strokeWeight(2);
  translate(0,translatey);
  // Calculating a1_a
  float upper = -g*(2*m1+m2)*sin(a1);
  upper -= m2*g*sin(a1-2*a2);
  upper -= 2*sin(a1-a2)*m2*(w2*w2*l2+w1*w1*l1*cos(a1-a2));
  float lower = l1*(2*m1+m2-m2*cos(2*a1-2*a2));
  a1_a = upper/lower;
  // Calculating a2_a
  upper = 2*sin(a1-a2);
  upper *= (w1*w1*l1*(m1+m2)+g*(m1+m2)*cos(a1)+w2*w2*l2*m2*cos(a1-a2));
  lower = l2*(2*m1+m2-m2*cos(2*a1-2*a2));
  a2_a = upper/lower;
  // Drag
  
  a1_a += drag * 0.5*(density*w1*w1*dragcoeff*m1/m1*normalarea)*opsign(w1);
  a2_a += drag * 0.5*(density*w2*w2*dragcoeff*m2/m1*normalarea)*opsign(w2);
  
  // Updating angular velocities  
  w1 += a1_a;
  w2 += a2_a;
  // Updating angles
  a1 += w1;
  a2 += w2;
  
  
  bob1.x = origin.x + l1 * sin(a1);
  bob1.y = origin.y + l1 * cos(a1);
  bob2.x = bob1.x   + l2 * sin(a2);
  bob2.y = bob1.y   + l2 * cos(a2);
  
  line(origin.x,origin.y,bob1.x,bob1.y);
  fill(0);
  ellipse(bob1.x,bob1.y,size1,size1);
  line(bob1.x,bob1.y,bob2.x,bob2.y);
  fill(0);
  ellipse(bob2.x,bob2.y,m2/m1*size1,m2/m1*size1);
  
  canvas.beginDraw();
  canvas.translate(0,translatey);
  canvas.strokeWeight(1);
  if(frameCount < 2) {
    canvas.point(bob2.x,bob2.y);
  }
  else{
    canvas.line(px2,py2,bob2.x,bob2.y);
  }
  canvas.endDraw();

  float v1 = l1*w1;
  float v2 = l2*w2;
  if(frameCount % 20 == 0) {
    print(0.5*m1*v1*v1 + 0.5*m2*v2*v2 + m1*g*bob1.y + m2*g*bob2.y,"\n");
  }
  px2 = bob2.x;
  py2 = bob2.y;
}
