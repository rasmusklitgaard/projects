PVector origin;
PVector bob;
float l = 190;
float a = PI/2;
float w = 0;
float g = 1;
float bobsize = 40;
void setup() {
   size(600,600);

   origin = new PVector(width/2,0);
   bob = new PVector(width/2,l);

}
void draw() {
  
  w += -g/l * sin(a);
  a += w;
   
  background(255);
  translate(0,300);
  bob.x = origin.x + l*sin(a);
  bob.y = origin.y + l*cos(a);
  line(origin.x,origin.y,bob.x,bob.y);
  fill(0);
  ellipse(bob.x,bob.y,bobsize,bobsize);
}
