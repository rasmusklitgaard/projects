from tkinter import *
import random
import numpy as np
import time
from math import sin,cos,pi

root = Tk()
root.title = "Game"
root.resizable(0,0)
root.wm_attributes("-topmost", 1)

size = (1024,768)
canvas = Canvas(root, width=size[0], height=size[1], bd=0, highlightthickness=0)
canvas.pack()

center = (size[0]/2,size[1]/2+0.1*size[1])
g = 1
l1 = 200
l2 = 200
m1 = 1
m2 = 1
r1 = 15
r2 = r1 * m2/m1

def ode_2pendulum(y): # should take y, and return y'
    yh0 = y[2]
    yh1 = y[3]
    yh2 = -g*(2*m1+m2)*sin(y[0]) -m2*g*sin(y[0]-2*y[1]) 
    -2*sin(y[0]-y[1])*m2*(y[3]**2 * l2 + y[2]**2 * l1*cos(y[0]-y[1]))
    yh2 /= l1 * (2*m1 + m2 - m2 * cos(2*y[0] - 2*y[1]))
    
    yh3 = 2*sin(y[0]-y[1]) * (y[2]**2*l1*(m1+m2) + g*(m1+m2)*cos(y[0]) 
                            + y[3]**2*l2*m2*cos(y[0]-y[1]))
    yh3 /= l2 * (2*m1 + m2 - m2 * cos(2*y[0] - 2*y[1])) 
    yres = np.array([yh0,yh1,yh2,yh3])
    return yres

def rk_step12(f, t, h, y): #  takes function f, which takes vector y as param.
    k1 = f(y);
    k2 = f(y + k1*(h*0.5))#, t + h*0.5)
    yh = y + k2*h
    ER = np.linalg.norm((k1 - k2)*(h*0.5))
    return yh, ER;

class Ball:
    def __init__(self, canvas, theta1, theta2, color = "red"):
        self.l1 = l1
        self.l2 = l2
        self.theta1 = theta1
        self.theta2 = theta2
        self.w1 = 0
        self.w2 = 0
        x1 = center[0] + l1 * sin(theta1)
        y1 = center[1] - l1 * cos(theta1)
        x2 = x1 + l2 * sin(theta2)
        y2 = y1 - l2 * cos(theta2)

        self.canvas = canvas
        self.line1 = canvas.create_line(center[0],size[1]-center[1],x1,size[1]-y1,width = 2)
        self.line2 = canvas.create_line(x1,size[1]-y1,x2,size[1]-y2,width = 2)

        self.ball1 = canvas.create_oval(-r1,-r1,r1,r1, fill=color)
        self.ball2 = canvas.create_oval(-r2,-r2,r2,r2, fill=color)

        self.canvas.move(self.ball1, x1, size[1]-y1)
        self.canvas.move(self.ball2, x2, size[1]-y2)

        centersquare = self.canvas.create_rectangle(-20,-20,20,20)
        self.canvas.move(centersquare,center[0],size[1]-center[1])

    def canvas_onclick(self, event):
        self.canvas.itemconfig(
            self.text_id,
            text="You clicked at ({}, {})".format(event.x, event.y)
        )

    def draw(self):
        coords = self.canvas.coords(self.ball1)
        x1 = center[0] + self.l1*sin(self.theta1)
        y1 = center[1] - self.l1*cos(self.theta1)
        x2 = x1 + self.l2*sin(self.theta2)
        y2 = y1 - self.l2*cos(self.theta2)

        err = 1
        tol = 1e-2
        h = 0.05
        t = 0
        newcoords = np.array([-1,-1,-1,-1])

        y = np.array([self.theta1,self.theta2,self.w1,self.w2])
        newcoords,err = rk_step12(ode_2pendulum,t,h,y)
        if(err > tol):
            h /= 2
            newcoords,err = rk_step12(ode_2pendulum,t,h,y)

        # Step taken, update angles.
        self.theta1 = newcoords[0]
        self.theta2 = newcoords[1]
        self.w1 = newcoords[2]
        self.w2 = newcoords[3]

        newx1 = center[0] + self.l1*sin(self.theta1)
        newy1 = center[1] - self.l1*cos(self.theta1)
        dx1 = newx1-x1
        dy1 = newy1-y1

        newx2 = newx1 + self.l2*sin(self.theta2)
        newy2 = newy1 - self.l2*cos(self.theta2)
        dx2 = newx2-x2
        dy2 = newy2-y2

        self.canvas.move(self.ball1, dx1, -dy1)
        self.canvas.move(self.ball2, dx2, -dy2)
        self.canvas.delete(self.line1)
        self.line1 = canvas.create_line(center[0],size[1]-center[1],newx1,size[1]-newy1,width = 2)
        self.canvas.delete(self.line2)
        self.line2 = canvas.create_line(newx1,size[1]-newy1,newx2,size[1]-newy2,width = 2)
        self.canvas.after(1, self.draw)




ball = Ball(canvas,pi/2,3*pi/4)
ball.draw()
root.mainloop()
