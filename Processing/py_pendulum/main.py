from tkinter import *
import random
import time
from math import sin,cos,pi

root = Tk()
root.title = "Game"
root.resizable(0,0)
root.wm_attributes("-topmost", 1)

size = (1024,768)
canvas = Canvas(root, width=size[0], height=size[1], bd=0, highlightthickness=0)
canvas.pack()

center = (size[0]/2,size[1]/2-0.1*size[1])
g = 1

class Ball:
    def __init__(self, canvas, theta, r = 15, l = (size[0]**2+size[1]**2)**0.5 * 0.13, color = "red"):
        self.l = l
        self.theta = theta
        self.w = 0
        x = center[0] + l * sin(theta)
        y = center[1] + l * cos(theta)
        self.canvas = canvas
        self.line = canvas.create_line(center[0],center[1],x,y,width = 5)
        self.id = canvas.create_oval(-r,-r,r,r, fill=color)
        self.canvas.move(self.id, x, y)
        self.radius=r

        centersquare = self.canvas.create_rectangle(-20,-20,20,20)
        self.canvas.move(centersquare,center[0],center[1])

#        self.canvas.bind("<Button-1>", self.canvas_onclick)
#        self.text_id = self.canvas.create_text(300, 200, anchor='se')
#        self.canvas.itemconfig(self.text_id, text='hello')

    def canvas_onclick(self, event):
        self.canvas.itemconfig(
            self.text_id,
            text="You clicked at ({}, {})".format(event.x, event.y)
        )

    def draw(self):
        coords = self.canvas.coords(self.id)
        x = center[0] + self.l*sin(self.theta)
        y = center[1] + self.l*cos(self.theta)

        self.w += -g/self.l * sin(self.theta)
        self.theta += self.w
        newx = center[0] + self.l*sin(self.theta)
        newy = center[1] + self.l*cos(self.theta)
        dx = newx-x
        dy = newy-y

        self.canvas.move(self.id, dx, dy)
        self.canvas.delete(self.line)
        self.line = canvas.create_line(center[0],center[1],newx,newy)
        self.canvas.after(25, self.draw)




ball = Ball(canvas,pi/2)
ball.draw()
root.mainloop()
