#include "textBox.hpp"

TextBox::TextBox(sf::String content,
                const sf::Vector2f& upperLeft,
                const sf::Vector2f lowerRight)
    : m_upperLeft (upperLeft), m_lowerRight (lowerRight)
{
    setString(content);
    setPosition(upperLeft);
    m_outline = sf::RectangleShape
                (sf::Vector2f(lowerRight.x - upperLeft.x,
                              lowerRight.y - upperLeft.y));

    m_outline.setPosition(upperLeft);

}

void TextBox::Render(const sf::RenderWindow& Window)
{
    //if the display list is current, just run it
	if (m_bIsDisplayListCurrent)
	{
	    glCallList(m_nDisplayListID);
		return;
	}
}