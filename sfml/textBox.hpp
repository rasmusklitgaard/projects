#include <SFML/Graphics.hpp>
#include <vector>

class TextBox : public sf::Text
{
public:

    TextBox(sf::String content = sf::String("content"),
            const sf::Vector2f& upperLeft = sf::Vector2f(0,0),
            const sf::Vector2f lowerRight = sf::Vector2f(0,0));

    void setUpperLeft(const sf::Vector2f& upperLeft);

    void setLowerRight(const sf::Vector2f& lowerRight);

protected:

    virtual void Render(const sf::RenderWindow& Window);

private:

    sf::Vector2f m_upperLeft;
    sf::Vector2f m_lowerRight;
    sf::RectangleShape m_outline;

    //Stores if the display list needs to be recompiled or not
	bool m_bIsDisplayListCurrent;
	//the ID for the OpenGL display list
	int m_nDisplayListID;
};