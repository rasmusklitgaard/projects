#include <stdio.h>
#include <math.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <SFML/Graphics.hpp>

int width = 1600;
int height = 900;
int blobsize = 20.f;
int centersize = 20.f;
int length = 330;
double mass = 10;

int cx = width/2-blobsize;
int cy = height/3-blobsize;

double theta = 0;;//M_PI/2;
double omega = 0;
double alpha = 0;

double g = 0.3;

double blobx =  cx + length * sin(theta);
double bloby =  cy + length * cos(theta);

double newx;
double newy;
double dx;
double dy;
double oldMousePosx;
double oldMousePosy;

double vx;
double vy;

double drag = 1;
double speed;
int opdir;

sf::Font font;

sf::Clock sfclock;
sf::Clock testclock;

sf::Time timeSinceLastFrame;

std::string inputString;
bool isInputActiveDrag = true;
bool isInputActiveLength = true;
bool writingToDrag = false;
bool writingToLength = false;

sf::Vector2f textBoxSize(130,80);
sf::Vector2f dragBoxPos(50,70);
sf::Vector2f lengthBoxPos(50,180);
void handleTextBoxEntry(sf::Event* event, sf::Text* textField, sf::String* str, bool* fieldActive, bool* writingToField)
{
    if (event->text.unicode == '\b') {
        if(str->getSize() > 0) {
            str->erase(str->getSize()- 1, 1);
            textField->setString(*str);
        }
        *fieldActive = false;
    }
    else if (event->text.unicode == 13) { // Che
        *fieldActive = true;
        if (str->getSize() == 0) {
            *str = "0.0";
            textField->setString(*str);
        }
        *writingToField = false;
    }
    else if (event->text.unicode < 128) {
        std::cout << "The raw input: " << event->text.unicode << std::endl;
        *str += static_cast<char>(event->text.unicode);
        textField->setString(*str);
        *fieldActive = false;
    }
    std::cout << "Text is entered \n";
}

void readyTextObj(sf::Text* text, sf::String str = sf::String("empty") ,sf::Vector2f pos = sf::Vector2f(0,0))
{
    text->setFillColor(sf::Color::Black);
    text->setFont(font);
    text->setPosition(pos);
    text->setString(str);
}
void makeTextBox(sf::RectangleShape* rect)
{
    rect->setFillColor(sf::Color::Transparent);
    rect->setSize(textBoxSize);
    rect->setOutlineColor(sf::Color::Black);
    rect->setOutlineThickness(2);
}

bool isInRectangle(sf::RectangleShape rect, int x, int y) 
{
    double rect_x = rect.getPosition().x;
    double rect_y = rect.getPosition().y;
    if(x >= rect_x && y >= rect_y && x <= (rect_x + rect.getSize().x) && y <= (rect_y + rect.getSize().y) ) {
        return true;
    }
    else {
        return false;
    }

}
bool isInBlob(int x, int y, double blobx, double bloby, int blobsize) 
{
    double norm = sqrt(pow(blobx-x,2) + pow(bloby-y,2));
    if(norm > blobsize)
        return false;
    else
        return true;
}
int opsign(double omega) 
{
    if(omega >= 0) 
        return 1;
    else
        return -1;
}
double calcspeed(double omega) 
{
    return length*omega;
}


int main()
{
    
    sf::RenderWindow window(sf::VideoMode(width, height), "My window", sf::Style::Close | sf::Style::Resize);
    window.setFramerateLimit(100);

    if(!font.loadFromFile("TravelingTypewriter.ttf")) 
    {
        return EXIT_FAILURE;
    }
    // Setup

    sf::Vertex line[] = 
        {
            sf::Vertex(sf::Vector2f(cx+blobsize,cy+blobsize),sf::Color::Black),
            sf::Vertex(sf::Vector2f(blobx+blobsize, bloby+blobsize),sf::Color::Black),
        };

    sf::CircleShape center(centersize);
    center.setFillColor(sf::Color::Black);

    sf::CircleShape blob1(blobsize);
    blob1.setFillColor(sf::Color::Blue);



    sf::String dragInput;
    sf::Text dragText;
    sf::Text dragBox;

    sf::String lengthInput;
    sf::Text lengthText;
    sf::Text lengthBox;

    sf::RectangleShape dragWindow(textBoxSize);
    dragWindow.move(dragBoxPos);
    makeTextBox(&dragWindow);

    sf::RectangleShape lengthWindow(textBoxSize);
    lengthWindow.move(lengthBoxPos);
    makeTextBox(&lengthWindow);

    readyTextObj(&dragText,sf::String("0.0"),sf::Vector2f(50,100));
    readyTextObj(&dragBox,sf::String("Drag:"),dragBoxPos);
    readyTextObj(&lengthText,sf::String("330"),sf::Vector2f(lengthBoxPos.x,lengthBoxPos.y+30));
    readyTextObj(&lengthBox,sf::String("Length:"),lengthBoxPos);

    blob1.move(blobx,bloby);
    center.move(cx,cy);

    // Setup
    // void resetAndDisplay(void) 
    // {
    //     window.clear(sf::Color::White);
    //     window.draw(center);
    //     window.draw(blob1);
    //     window.draw(dragText);
    //     window.draw(dragBox);
    //     window.display();
    // }
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            switch (event.type)
            {
            case sf::Event::Closed:
                window.close();
                break;
            case sf::Event::Resized:
                printf("Rezised window: %i x %i\n",window.getSize().x,window.getSize().y);
                break;
            case sf::Event::TextEntered:
                if(writingToDrag)
                {
                    handleTextBoxEntry(&event, &dragText, &dragInput, &isInputActiveDrag, &writingToDrag);
                }
                else if (writingToLength)
                {
                    handleTextBoxEntry(&event, &lengthText, &lengthInput, &isInputActiveLength, &writingToLength);
                }
                

            default:
                ;
            }
            if (event.type == sf::Event::Closed)
                window.close();
        }
        if(sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
            int x = sf::Mouse::getPosition(window).x;
            int y = sf::Mouse::getPosition(window).y;

            
            sfclock.restart();
            oldMousePosx = (double) x;
            oldMousePosy = (double) y;
            if(isInBlob(x,y,blobx,bloby,blobsize)) {
                while(sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
                    timeSinceLastFrame = sfclock.getElapsedTime();
                    sfclock.restart();
                    x = sf::Mouse::getPosition(window).x;
                    y = sf::Mouse::getPosition(window).y;
                    
                    vx = (x - oldMousePosx) / (double) timeSinceLastFrame.asMilliseconds();
                    vy = (y - oldMousePosy) / (double) timeSinceLastFrame.asMilliseconds();

                    if(sin(theta) == 0) {
                        omega = -vy / length;
                    }
                    else if(cos(theta) == 0) {
                        omega = vx / length;
                    }
                    else {
                        omega = vx / sin(theta) / length;
                    }
                    

                    oldMousePosx = (double) x;
                    oldMousePosy = (double) y;
                    theta = atan2(-( (double) cy - (double) y ), (double) cx - (double) x) - M_PI/2;
                    dx = x - blobx;
                    dy = y - bloby;

                    newx = x;
                    newy = y;

                    blobx = x;
                    bloby = y;

                    blob1.move(dx,dy);

                    // resetAndDisplay();
                    window.clear(sf::Color::White);
                    window.draw(center);
                    window.draw(dragWindow);
                    window.draw(blob1);
                    window.draw(dragText);
                    window.draw(dragBox);
                    window.draw(lengthText);
                    window.draw(lengthWindow);
                    window.draw(lengthBox);
                    window.display();

                }
                // Now blob is released
                alpha = 0;
                //omega = 0;

/* 
                while(cy + length * sin(theta) - y > 0.01 * blobsize || cx + length * cos(theta - x > 0.01 * blobsize)) {
                    theta += M_PI / 2;
                }
*/


                length = sqrt((cx-x)*(cx-x) + (cy-y)*(cy-y));
                isInputActiveLength = false;
                lengthText.setString(sf::String(std::to_string(length)));
                std::cout << "theta = " << theta << std::endl;
                std::cout << "cx-x, cy-y:" << cx-x << "," << cy-y << std::endl;
                printf("Now x,y = %g, %g\n",blobx,bloby);
            }
            if (isInRectangle(dragWindow,x,y) && sf::Mouse::isButtonPressed(sf::Mouse::Left)) 
            {
                writingToDrag = true;
            }
            else if(isInRectangle(lengthWindow,x,y) && sf::Mouse::isButtonPressed(sf::Mouse::Left))
            {
                writingToLength = true;
            }
        }

//        sfclock.restart();
        // Do calculations here

// TODO::: 
// FIX DET HER INPUT MED DRAG
        opdir = opsign(omega);
        speed = calcspeed(omega);

        alpha = -g/length * sin(theta) - drag / mass * 0.000002 * pow( (speed), 2 ) * opdir;
        omega += alpha;
        if(isInputActiveDrag && dragInput.getSize() > 0) {
            
            inputString = dragInput.toAnsiString();

            try
            {
                drag = stof(inputString);
            }
            catch(const std::exception& e)
            {
                std::cerr << e.what() << '\n';
            }
        }
        else if (isInputActiveDrag){
            drag = 0.0;
        }
        
        if(isInputActiveLength && lengthInput.getSize() > 0) {
            
            inputString = lengthInput.toAnsiString();

            try
            {
                length = stoi(inputString);
            }
            catch(const std::exception& e)
            {
                std::cerr << e.what() << '\n';
            }
        }
        else if (isInputActiveLength){
            length = 330;
        }
        std::cout << "length is now: " << length << std::endl;


        // omega *= drag;
        theta += omega;
 
        newx = cx + length * sin(theta);
        newy = cy + length * cos(theta);

        dx = newx-blobx;
        dy = newy-bloby;

        blobx = newx;
        bloby = newy;


        
        // Do calculations here
//        sf::Time usedtime = sfclock.getElapsedTime();

        // Do movement here
//        dx = -sin(theta)*omega*length*usedtime.asMicroseconds();
//        dy = cos(theta)*omega*length*usedtime.asMicroseconds(); 
        blob1.move(dx,dy);
        line[1] = sf::Vertex(sf::Vector2f(blobx+blobsize,bloby+blobsize),sf::Color::Black);


        // Do movement here
        isInputActiveDrag? dragText.setFillColor(sf::Color::Green) : dragText.setFillColor(sf::Color::Red);
        
        // resetAndDisplay();
        window.clear(sf::Color::White);
        window.draw(center);
        window.draw(blob1);
        window.draw(dragText);
        window.draw(dragWindow);
        window.draw(dragBox);
        window.draw(lengthText);
        window.draw(lengthWindow);
        window.draw(lengthBox);
        window.draw(line,2,sf::Lines);
        window.display();
    }




    
    return 0;
}
