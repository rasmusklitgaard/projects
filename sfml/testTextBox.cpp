#include "textBox.hpp"
#include <SFML/Graphics.hpp>
#include <iostream>

int main() 
{
    sf::RenderWindow window(sf::VideoMode(400,400), "Test window", sf::Style::Close | sf::Style::Resize);
    
    sf::Font font;
    if(!font.loadFromFile("TravelingTypewriter.ttf")) {
        return EXIT_FAILURE;
    }

    TextBox textBox(sf::String("Test"),
                    sf::Vector2f(20,20),
                    sf::Vector2f(120,120));
    textBox.setFont(font);
    textBox.setFillColor(sf::Color::Black);
    textBox.setPosition(50,50);

    while(window.isOpen())
    {
sf::Event event;
        while (window.pollEvent(event))
        {
            switch (event.type)
            {
            case sf::Event::Closed:
                window.close();
                break;
            case sf::Event::Resized:
                break;
            default:
                ;
            }
        }



        window.clear(sf::Color::White);
        window.draw(textBox);
        window.display();
    }
    return 0;
}