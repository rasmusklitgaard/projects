#include<string.h>
#include<stdio.h>
#include<stdlib.h>
char* morse(char* letter)
{
		if(strcmp(letter, "a")==0) return ".-";
		else if(strcmp(letter, "b")==0) return "-...";
		else if(strcmp(letter, "c")==0) return "-.-.";
		else if(strcmp(letter, "d")==0) return "-..";
		else if(strcmp(letter, "e")==0) return ".";
		else if(strcmp(letter, "f")==0) return "..-.";
		else if(strcmp(letter, "g")==0) return "--.";
		else if(strcmp(letter, "h")==0) return "....";
		else if(strcmp(letter, "i")==0) return "..";
		else if(strcmp(letter, "j")==0) return ".---";
		else if(strcmp(letter, "k")==0) return "-.-";
		else if(strcmp(letter, "l")==0) return ".-..";
		else if(strcmp(letter, "m")==0) return "--";
		else if(strcmp(letter, "n")==0) return "-.";
		else if(strcmp(letter, "o")==0) return "---";
		else if(strcmp(letter, "p")==0) return ".--.";
		else if(strcmp(letter, "q")==0) return "--.-";
		else if(strcmp(letter, "r")==0) return ".-.";
		else if(strcmp(letter, "s")==0) return "...";
		else if(strcmp(letter, "t")==0) return "-";
		else if(strcmp(letter, "u")==0) return "..-";
		else if(strcmp(letter, "v")==0) return "...-";
		else if(strcmp(letter, "w")==0) return ".--";
		else if(strcmp(letter, "x")==0) return "-..-";
		else if(strcmp(letter, "y")==0) return "-.--";
		else if(strcmp(letter, "z")==0) return "--..";
		else if(strcmp(letter, "æ")==0) {morse("a"); morse("e");}
		else if(strcmp(letter, "ø")==0) {morse("o"); morse("e");}
		else if(strcmp(letter, "å")==0) {morse("a"); morse("a");}
		else if(strcmp(letter, " ")==0) return "      ";
		else if(strcmp(letter, "1")==0) return ".----";
		else if(strcmp(letter, "2")==0) return "..---";
		else if(strcmp(letter, "3")==0) return "...--";
		else if(strcmp(letter, "4")==0) return "....-";
		else if(strcmp(letter, "5")==0) return ".....";
		else if(strcmp(letter, "6")==0) return "-....";
		else if(strcmp(letter, "7")==0) return "--...";
		else if(strcmp(letter, "8")==0) return "---..";
		else if(strcmp(letter, "9")==0) return "----.";
		else if(strcmp(letter, "0")==0) return "-----";
	exit(0);
}
int main(int argc, char** argv)
{
	if(argc>1){
	printf("this letter is %s\n",&argv[1][0]);
	printf("%s",morse(&argv[1][0]));
	printf("\n");
	}	
	return 0;
}
