#include<string.h>
#include<stdio.h>
#include<stdlib.h>
char* morse(char* letter)
{
		if(strncmp(letter, "a",1)==0) return ".-";
		else if(strncmp(letter, "b",1)==0) return "-...";
		else if(strncmp(letter, "c",1)==0) return "-.-.";
		else if(strncmp(letter, "d",1)==0) return "-..";
		else if(strncmp(letter, "e",1)==0) return ".";
		else if(strncmp(letter, "f",1)==0) return "..-.";
		else if(strncmp(letter, "g",1)==0) return "--.";
		else if(strncmp(letter, "h",1)==0) return "....";
		else if(strncmp(letter, "i",1)==0) return "..";
		else if(strncmp(letter, "j",1)==0) return ".---";
		else if(strncmp(letter, "k",1)==0) return "-.-";
		else if(strncmp(letter, "l",1)==0) return ".-..";
		else if(strncmp(letter, "m",1)==0) return "--";
		else if(strncmp(letter, "n",1)==0) return "-.";
		else if(strncmp(letter, "o",1)==0) return "---";
		else if(strncmp(letter, "p",1)==0) return ".--.";
		else if(strncmp(letter, "q",1)==0) return "--.-";
		else if(strncmp(letter, "r",1)==0) return ".-.";
		else if(strncmp(letter, "s",1)==0) return "...";
		else if(strncmp(letter, "t",1)==0) return "-";
		else if(strncmp(letter, "u",1)==0) return "..-";
		else if(strncmp(letter, "v",1)==0) return "...-";
		else if(strncmp(letter, "w",1)==0) return ".--";
		else if(strncmp(letter, "x",1)==0) return "-..-";
		else if(strncmp(letter, "y",1)==0) return "-.--";
		else if(strncmp(letter, "z",1)==0) return "--..";
		else if(strncmp(letter, " ",1)==0) return "      ";
		else if(strncmp(letter, "1",1)==0) return ".----";
		else if(strncmp(letter, "2",1)==0) return "..---";
		else if(strncmp(letter, "3",1)==0) return "...--";
		else if(strncmp(letter, "4",1)==0) return "....-";
		else if(strncmp(letter, "5",1)==0) return ".....";
		else if(strncmp(letter, "6",1)==0) return "-....";
		else if(strncmp(letter, "7",1)==0) return "--...";
		else if(strncmp(letter, "8",1)==0) return "---..";
		else if(strncmp(letter, "9",1)==0) return "----.";
		else if(strncmp(letter, "0",1)==0) return "-----";
	return "No clause triggered, exiting";
}
