#include<iostream>
#include<cstdlib>
#include<math.h>
#include<assert.h>

using namespace std;

class Vector {
	public:
		int size;
		double* data;

		Vector(int i);
		~Vector();
		void add(Vector b);
		void sub(Vector b);
		double scalarProduct(Vector b);
		double angle(Vector b);
		double norm();
		void print();
		void set(int i, double x);
		void set_all(double x);
		void scale(double x);
};
Vector::Vector(int i) {
	size = i;
	data = (double*) malloc(size*sizeof(double));
}
Vector::~Vector() {
	free(data);
}
void Vector::add(Vector b) {
	assert(b.size == size);
	for(int i = 0; i < size; i++) {
		data[i] += b.data[i];	
	}
}
void Vector::sub(Vector b) {
	assert(b.size == size);
	for(int i = 0; i<size; i++) {
		data[i] -= b.data[i];
	}
}
double Vector::scalarProduct(Vector b) {
	assert(b.size == size);
	double rv = 0;
	for(int i = 0; i<size; i++) {
		rv += data[i]*b.data[i];
	}
	return rv;
}
double Vector::angle(Vector b) {
	double rv = (*this).scalarProduct(b) / (*this).norm() / b.norm();
	return acos(rv);
}
double Vector::norm() {
	double rv = 0;
	for(int i = 0; i<size; i++) {
		rv += data[i]*data[i];
	}
	return sqrt(rv);
}
void Vector::print() {
	cout << "Vector of size (1," << size << ")" << endl;
	cout << "__________" << endl;
	for(int i = 0; i<size; i++) {
		cout << data[i] << endl;
	}
	cout << "__________" << endl;
}
void Vector::set(int i, double x) {
	assert(i <= size - 1);
	data[i] = x;
}
void Vector::set_all(double x) {
	for(int i = 0; i<size; i++) {
		(*this).set(i,x);
	}
}
void Vector::scale(double x) {
	for(int i = 0; i<size; i++) {
		data[i] *= x;
	}
}
