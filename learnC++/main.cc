#include<iostream>
#include<cstdlib>
#include<math.h>
#include<assert.h>
#include"vector.h"
using namespace std;

int main() {
	Vector a(2);
	a.set_all(1);
	a.set(1,2);

	Vector b(2);
	b.set(0,-2);
	b.set(1,1);
	
	a.print();
	b.print();

	cout << "||a|| = " << a.norm() << endl;
	cout << "a*b = " << a.scalarProduct(b) << endl;
	return 0;
}
