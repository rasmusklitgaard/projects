class Vector {
	public:
		int size;
		double* data;
		void add(Vector b);
		void sub(Vector b);
		double scalarProduct(Vector b);
		double angle(Vector b);
		double norm();
		void print();
		void set(int i, double x);
		void set_all(double x);
		Vector(int i);
		~Vector();
};
