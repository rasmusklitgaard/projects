#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include <stdbool.h>

int main(int argc, char** argv) {
	long long n = 0;
	int flag = 0;
	if(argc > 1) {
		n =  atoll(argv[1]);
	}
	else return 0;
	int breaker = 0;
	#pragma omp parallel for reduction(||:flag) shared(breaker)
	for(long long i = 2; i< n; i++) 
	{
		if(breaker == 1){continue;} 
		double mod = fmodl( (long double) n, (long double) i );
		if(mod == 0)
		{
			if(breaker !=1){
				breaker = 1;
				printf("%lld is not a prime number\n",n);
			}
			flag = 1;
		}
		
	}
	if(flag != 1)
	{
		printf("%lld is a prime number\n",n);
	}
	return 0;
}
