#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include <stdbool.h>

/*
int main(int argc, char** argv) {
	long long n=0;
	if(argc > 1){
		n = (long long) atof(argv[1]);
	}
	if(n < 0) {
		printf("%lld = -1",n);
		n = -n;
	}
	else if(n == 0){
		exit(0);
	}
	else if(n >0){
		printf("%lld = 1",n);
	}
	int flag = 0;
	for(long long i = 2; i<=n;flag == 0? i++ : i)
	{
		flag = 0;
		if (fmodl((long double) n,(long double) i) == 0)  
		{
			printf(" * %lld", i);
			n = n/i;
			flag = 1;
		}
	}
	printf("\n");

	return 0;
}
*/

int main(int argc, char** argv) {
	long double n=0;
	if(argc > 1){
		n = (long double) atoll(argv[1]);
	}
	if(n < 0) {
		printf("%lld = -1",(long long) n);
		n = -n;
	}
	else if(n == 0){
		exit(0);
	}
	else if(n >0){
		printf("%lld = 1",(long long) n);
	}
	int flag = 0;
	for(long double i = 2; i<=n;flag == 0? i++ : i)
	{
		flag = 0;
		if (fmodl((long double) n,(long double) i) == 0)  
		{
			printf(" * %lld",(long long) i);
			n = n/i;
			flag = 1;
		}
	}
	printf("\n");

	return 0;
}
